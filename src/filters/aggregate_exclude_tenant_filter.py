from oslo_log import log as logging

from nova.scheduler import filters
from nova.scheduler.filters import utils


LOG = logging.getLogger(__name__)

def toto():
    print("coucou")

class AggregateExcludeTenant(filters.BaseHostFilter):
    """Exclude Tenant according to Aggregate"""

    # Aggregate data and tenant do not change within a request
    run_filter_once_per_request = True

    RUN_ON_REBUILD = False

    def host_passes(self, host_state, spec_obj):
        """If a host is in an aggregate that has the metadata key
        "exclude_tenant_id" it prevents to create instances from that tenant(s).
        A host can be in different aggregates.

        """
        tenant_id = spec_obj.project_id

        metadata = utils.aggregate_metadata_get_by_host(host_state,
                                                        key="exclude_tenant_id")

        if metadata != {}:
            configured_tenant_ids = metadata.get("exclude_tenant_id")
            if configured_tenant_ids:
                if tenant_id in configured_tenant_ids:
                    LOG.debug("%s fails tenant id on aggregate", host_state)
                    return False
                LOG.debug("Host tenant id %s matched", tenant_id)
            else:
                LOG.debug("No tenant id's defined on host. Host passes.")
        return True
