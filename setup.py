#!/usr/bin/python
# -*-coding:UTF-8 -*
from distutils.core import setup


setup(
    name='dptscheduler',
    description='Department own nova filter',
    version='1.0.0',
    author='Romain Chanu',
    author_email='romain.chanu@univ-lyon1.fr',
    #py_modules=['AggregateExcludeTenant']
    package_dir={'dptscheduler':'src/filters'},
    packages=['dptscheduler'],
)

